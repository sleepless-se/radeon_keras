FROM rastasheep/ubuntu-sshd:16.04
COPY . /docker

# keras setting file for PlaidML
RUN mkdir ~/.keras && mv /docker/docker-keras.json  ~/.keras/

# install applications
RUN apt-get update && apt-get install -y build-essential wget make libffi-dev mesa-opencl-icd clinfo openssh-server checkinstall libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev libssl-dev zlib1g-dev openssl libffi-dev python3-dev python3-setuptools wget zlib1g-dev


# install python3.7
RUN wget https://www.python.org/ftp/python/3.7.1/Python-3.7.1.tgz && \
    tar -xzf Python-3.7.1.tgz && \
    cd Python-3.7.1 && \
    ./configure && \
    make && \
    make install

# install requirements.txt
RUN pip3 install -r /docker/requirements.txt


RUN wget --referer=http://support.amd.com https://www2.ati.com/drivers/linux/beta/ubuntu/amdgpu-pro-17.40.2712-510357.tar.xz && \
    tar -Jxvf amdgpu-pro-17.40.2712-510357.tar.xz && \
    cd amdgpu-pro-17.40.2712-510357 && \
    chmod u+x ./amdgpu-pro-install && \
    ./amdgpu-pro-install --compute -y

WORKDIR /docker
EXPOSE 22
# set ssh user:password
RUN echo 'root:root' | chpasswd